import { NgModule } from '@angular/core';
import { PrivacyRoutes } from './privacy.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { PrivacyComponent } from './privacy.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PrivacyRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [PrivacyComponent]
})
export class PrivacyModule { }

