import { Routes, RouterModule } from '@angular/router';
import { PrivacyComponent } from './privacy.component';

export const PrivacyRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: PrivacyComponent
    }]
}];
