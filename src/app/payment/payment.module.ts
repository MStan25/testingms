import { NgModule } from '@angular/core';
import { PaymentRoutes } from './payment.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { PaymentComponent } from './payment.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PaymentRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [PaymentComponent]
})
export class PaymentModule { }

