import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { AppComponent } from './app.component';

import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FixedpluginModule } from './shared/fixedplugin/fixedplugin.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SidebarComponent } from './sidebar/sidebar.component'
import { AppRoutes } from './app.routing';


import { CoreModule } from './core/core.module';

import { AppSettingsModule } from './app.settings';
import { niAuthService } from './core/services/niAuthService';
import { AuthService } from './core/services/auth.service';
import { LoginService } from './core/services/login.service';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { RegisterModalComponent } from 'app/races/register-modal/register-modal.component';
import { RaceModalComponent } from 'app/races/detail/race-modal/race-modal.component';
import { SidebarUserModalComponent } from 'app/sidebar/sidebar-user-modal/sidebar-user-modal.component';

import { DatedropdownComponent } from 'app/core/components/datedropdown/datedropdown.component';
import { SidebarModule } from 'app/sidebar/sidebar.module';
import { UserComponent } from 'app/core/components/user/user-detail.component';
// import { StaticModule } from './static/static.module';
// import { FeatureModule } from './feature-module/feature.module';
// import { SharedModule } from './shared/shared.module';

import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

let config = new AuthServiceConfig([
      {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("393151204352-3bbvejda5n9ki32c40nl4lqq61chj20o.apps.googleusercontent.com")
      },
      {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("ovdje ide facebook app-id")
      }
  ]);
  
  export function provideConfig() {
      return config;
  }

@NgModule({
   exports: [
      MatAutocompleteModule,
      MatButtonModule,
      MatButtonToggleModule,
      MatCardModule,
      MatCheckboxModule,
      MatChipsModule,
      MatStepperModule,
      MatDatepickerModule,
      MatDialogModule,
      MatExpansionModule,
      MatGridListModule,
      MatIconModule,
      MatInputModule,
      MatListModule,
      MatMenuModule,
      MatNativeDateModule,
      MatPaginatorModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatRadioModule,
      MatRippleModule,
      MatSelectModule,
      MatSidenavModule,
      MatSliderModule,
      MatSlideToggleModule,
      MatSnackBarModule,
      MatSortModule,
      MatTableModule,
      MatTabsModule,
      MatToolbarModule,
      MatTooltipModule
   ],
   declarations: [
   ]
})
export class MaterialModule { }

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    MatNativeDateModule,
    NavbarModule,
    FooterModule,
    FixedpluginModule,
    CoreModule.forRoot(),
    HttpClientModule,
    SidebarModule,
    AppSettingsModule.forRoot(),
    RouterModule.forRoot(AppRoutes, { useHash: true, enableTracing: false,anchorScrolling: 'enabled', }),
    SocialLoginModule
  ],
  providers: [
    niAuthService,
    AuthService, 
    LoginService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptor,
        multi: true
    }
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    SidebarUserModalComponent,
    DatedropdownComponent,
    UserComponent,
    RegisterModalComponent

    
  ],
  entryComponents: [SidebarUserModalComponent,RegisterModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
