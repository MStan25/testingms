import { NgModule } from '@angular/core';
import { ContactRoutes } from './contact.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { ContactComponent } from './contact.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ContactRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [ContactComponent]
})
export class ContactModule { }

