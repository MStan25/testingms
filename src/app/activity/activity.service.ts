import { Injectable } from '@angular/core'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { MasterService } from '../core/services/master.service'

@Injectable()
 export class ActivityService {
    constructor(private service: MasterService) { }

    getUpcomingRaces = function (filter) {
      return this.service.niPostPromise("api/races/filter/racesbystatus", filter);
    }
}