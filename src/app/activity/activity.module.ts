import { UpcomingRacesComponent } from './upcoming-races/upcoming-races.component';
import { FinishedRacesComponent } from './finished-races/finished-races.component';

import { NgModule } from '@angular/core';
import { ActivityRoutes } from './activity.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { ActivityComponent } from './activity.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ActivityRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [
    ActivityComponent,
    FinishedRacesComponent,
    UpcomingRacesComponent
  ]
})
export class ActivityModule { }

