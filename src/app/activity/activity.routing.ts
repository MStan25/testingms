import { Routes, RouterModule } from '@angular/router';
import { ActivityComponent } from './activity.component';
import { FinishedRacesComponent } from './finished-races/finished-races.component';
import { UpcomingRacesComponent } from './upcoming-races/upcoming-races.component';

export const ActivityRoutes: Routes = [{
    path: '',
    children: [{
        path: 'activity',
        component: UpcomingRacesComponent
    }, 
    {
        path: 'upcomingRaces',
        component: UpcomingRacesComponent
    },
    {
        path: 'finishedRaces',
        component: FinishedRacesComponent
    },
    ]
}];
