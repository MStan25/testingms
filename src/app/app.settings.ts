import { NgModule, ModuleWithProviders } from '@angular/core';


@NgModule({})
export class AppSettingsModule {

  //public static ServiceHost = 'http://dev.novi-is.com/stotinkaapi/';
  public static ServiceHost = 'http://localhost:4567/';
  public static AppVersion = "0.0.1";


  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppSettingsModule,
      providers: []
    };
  }


} 