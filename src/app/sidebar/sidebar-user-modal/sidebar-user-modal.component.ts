import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { UserComponent } from '../../core/components/user/user-detail.component'
import { DatedropdownComponent } from '../../core/components/datedropdown/datedropdown.component';

@Component({
  selector: 'app-sidebar-user-modal',
  templateUrl: './sidebar-user-modal.component.html',
  styleUrls: ['./sidebar-user-modal.component.css']
})
export class SidebarUserModalComponent implements OnInit {


  currentWindowWidth: number;
  isProfileLoading: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<SidebarUserModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;

  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;

  }

  closeForm() {
    this.dialogRef.close();
  }



}



