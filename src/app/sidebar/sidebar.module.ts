import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar.component';
import { SidebarUserModalComponent } from './sidebar-user-modal/sidebar-user-modal.component';
import { DatedropdownComponent } from '../core/components/datedropdown/datedropdown.component';
import { UserComponent } from '../core/components/user/user-detail.component'
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        MatSelectModule,
        MatTooltipModule
       
        
        ],
    declarations: [
        SidebarComponent
    ],
    exports: [SidebarComponent]
})

export class SidebarModule { }


