import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { EventRoutes } from './event.routing';
import { SharedModule } from '../core/components/shared.module'
import { EventComponent } from './event.component';
// import { DatedropdownComponent } from './../core/components/datedropdown/datedropdown.component';
import { MatSelectModule } from '@angular/material';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EventRoutes),
    FormsModule,
    MaterialModule,
    MatSelectModule,
    SharedModule
  ],
  declarations: [EventComponent],
  entryComponents: [
  ]
})
export class EventModule { }

