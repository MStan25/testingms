import { Injectable, Component } from '@angular/core'
import { Http, Response } from '@angular/http'
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/Observable'
import { MasterService } from '../core/services/master.service'

@Injectable()
 export class EventService {
    constructor(private service: MasterService) { }

    getOrganiser = function (filter) {
        return this.service.niPostPromise("api/races/organiser", filter);
    }
    getRaceRegistrationDetail = function (filter) {
        return this.service.niPostPromise("api/races/raceregistrationdetail", filter);
    }
    getRaceRegistrationCountry = function (filter) {
        return this.service.niPostPromise("api/races/raceregistrationcountry", filter);
    }
    
    getRecommendedRaces = function (filter) {
        return this.service.niPostPromise("api/races/filter/recommended", filter);
    }
    
    registerToRace = function (filter) {
        return this.service.niPostPromise("api/races/registertorace", filter);
    }
    
    getRaceRegistrationComponents = function (filter) {
        return this.service.niPostPromise("api/formparameterization/getbyraceregistration", filter);
    }

    getUserProfile = function (filter) {
        return this.service.niPostPromise("api/user/userprofile", filter);
    }

    getRaceCompetitors = function (filter) {
        return this.service.niPostPromise("api/races/getracecompetitors", filter);
    }

    getResults = function (filter) {
        return this.service.niPostPromise("api/races/filterraceresults", filter);
    }
}