import { NgModule } from '@angular/core';
import { HelpRoutes } from './help.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { HelpComponent } from './help.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HelpRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [HelpComponent]
})
export class HelpModule { }

