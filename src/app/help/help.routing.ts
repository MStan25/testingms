import { Routes, RouterModule } from '@angular/router';
import { HelpComponent } from './help.component';

export const HelpRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: HelpComponent
    }]
}];
