import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './core/guards/auth.guard';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }, {
    path: '',
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule',
        canActivate: [AuthGuard]
      }, {
        path: 'user',
        loadChildren: './user/user.module#UserModule',
        canActivate: [AuthGuard]
      }
      , {
        path: 'sport',
        loadChildren: './sports/sports.module#SportsModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'activity',
        loadChildren: './activity/activity.module#ActivityModule',
        canActivate: [AuthGuard]
      }
      ,
      {
        path: 'payment',
        loadChildren: './payment/payment.module#PaymentModule',
        canActivate: [AuthGuard]
      }
      ,
      {
        path: 'help',
        loadChildren: './help/help.module#HelpModule',
        canActivate: [AuthGuard]
      }
      ,
      {
        path: 'about',
        loadChildren: './about/about.module#AboutModule',
        canActivate: [AuthGuard]
      }
      ,
      {
        path: 'contact',
        loadChildren: './contact/contact.module#ContactModule',
        canActivate: [AuthGuard]
      }
      ,
      {
        path: 'privacy',
        loadChildren: './privacy/privacy.module#PrivacyModule',
        canActivate: [AuthGuard]
      },
      {
        path: 'event',
        loadChildren: './event/event.module#EventModule',
        canActivate: [AuthGuard]
      }

      // {
      //     path: 'forms',
      //     loadChildren: './forms/forms.module#Forms'
      // }, {
      //     path: 'tables',
      //     loadChildren: './tables/tables.module#TablesModule'
      // }, {
      //     path: 'maps',
      //     loadChildren: './maps/maps.module#MapsModule'
      // }, {
      //     path: 'widgets',
      //     loadChildren: './widgets/widgets.module#WidgetsModule'
      // }, {
      //     path: 'charts',
      //     loadChildren: './charts/charts.module#ChartsModule'
      // }, {
      //     path: 'calendar',
      //     loadChildren: './calendar/calendar.module#CalendarModule'
      // }, 
      // {
      //     path: '',
      //     loadChildren: './userpage/user.module#UserModule'
      // }
    ]
  }, {
    path: '',
    component: AuthLayoutComponent,
    children: [{
      path: 'login',
      loadChildren: './pages/pages.module#PagesModule'
    }]
  }
];

