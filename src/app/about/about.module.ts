
import { NgModule } from '@angular/core';
import { AboutRoutes } from './about.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { AboutComponent } from './about.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AboutRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [AboutComponent]
})
export class AboutModule { }

