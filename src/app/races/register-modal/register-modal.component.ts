
import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs/Rx';

import { NotificationService } from '../../core/services/notification.service';
import { EventService } from '../../event/event.service';

declare var $: any;
@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css'],
  providers: [EventService, NotificationService]
})
export class RegisterModalComponent implements OnInit {

  currentWindowWidth: number;
  isFormLoading: boolean = true;
  registration: any = {};
  fields: any = {};
  codebooks: any;
  isProfileLoading: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<RegisterModalComponent>,
    private eventService: EventService,
    private notify: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;
    this.registration = {};
    this.codebooks = this.data['codebooks'];
    this.getUserProfile();
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;

  }

  closeForm() {
    this.dialogRef.close();
  }

  getUserProfile() {
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));

    var request = {};
    request["userId"] = session["id"];
    Observable.from(this.eventService.getUserProfile(request)).subscribe((result: any) => {
      if (result != null) {

        this.registration['FirstName'] = result['firstname'];
        this.registration['LastName'] = result['lastname'];
        this.registration['Email'] = result['email'];
        this.registration['MobilePhone'] = result['phoneNumber'];
           this.registration['DateOfBirth'] = result['birthDay'];
        this.registration['PlaceOfResidence'] = result['city'];
        this.registration['Address'] = result['address'];
        this.registration['Gender'] = result['gender'];

        this.isProfileLoading = false;
        this.loadRaceRegistrationComponents();

      }
    });
  }

  loadRaceRegistrationComponents() {
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));

    var request = {};
    request["raceId"] = this.data["id"];
    request["userId"] = session["id"];
    this.isFormLoading = true;
    this.isAdditionalFieldsLoading = true;
    Observable.from(this.eventService.getRaceRegistrationComponents(request)).subscribe((result: any) => {
      if (result != null) {
        this.fields = result;
        if (this.fields.length > 0) {
          this.hasAdditionalFields = true;
        } else {
          this.hasAdditionalFields = false;
        }
        this.renderComponents();

        this.isFormLoading = false;
      } else {
        this.fields = [];
        this.hasAdditionalFields = false;
        this.isFormLoading = false;
      }

    });
  }

  hasAdditionalFields: boolean = true;
  isAdditionalFieldsLoading: boolean = true;
  isSendingRequest: boolean = false;
  renderComponents() {
    this.fields.forEach(element => {

      if(element.fieldName != 'FirstName' &&
      element.fieldName != 'LastName' &&
      element.fieldName != 'Email' &&
      element.fieldName != 'MobilePhone' &&
      element.fieldName != 'DateOfBirth' &&
      element.fieldName != 'PlaceOfResidence' &&
      element.fieldName != 'PlaceOfResidence' &&
      element.fieldName != 'Address' &&
      element.fieldName != 'Gender'
    ){

      this.registration[element.fieldName] = "";
    } 
    this.registration[element.fieldName + "Mandatory"] = element.mandatory;

    });

    setTimeout(() => {

      this.isAdditionalFieldsLoading = false;
    }, 1200);

  }

  registerToRace() {
    
    this.isSendingRequest = true;
    if (!this.validateRequest()) {
      this.notify.error("Popunite obavezna polja");
      this.isSendingRequest = false;
      return;
    }
    var request = this.registration;
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
    request["RaceId"] = this.data["id"];
    request["UserId"] = session["id"];

    if (request['DateOfBirth'] != null) {
      request['DayOfBirth'] = request['DateOfBirth'].getDate();
      request['MonthOfBirth'] = request['DateOfBirth'].getMonth();
      request['YearOfBirth'] = request['DateOfBirth'].getFullYear();
    }

    Observable.from(this.eventService.registerToRace(request)).subscribe((result: any) => {
      this.isSendingRequest = false;
      if (result != null && result == true) {
        this.notify.success("Uspiješno ste se prijavili na utrku!");
        this.dialogRef.close();
      } else {
        if(result.resultCode = 10){
          this.notify.info("Već ste prijavljeni na odabranu utrku!");
        }else{
          this.notify.error("Došlo je do greške!");
        }
      }

    });

  }

  dateOfBirthValid(value: any) {

  }

  validateRequest() {
    var isValid = true;
    debugger;
    this.fields.forEach(prop => {
      if (prop['mandatory'] == true) {
        if (prop['fieldName'].toUpperCase() == "YEAROFBIRTH" || prop['fieldName'].toUpperCase() == "MONTHOFBIRTH" || prop['fieldName'].toUpperCase() == "DAYOFBIRTH") {
   
          if (this.registration['DateOfBirth'] == undefined || this.registration['DateOfBirth'] == null || this.registration['DateOfBirth'] == "") {
            isValid = false;
          }

        } else {
          if (this.registration[prop['fieldName']] == undefined || this.registration[prop['fieldName']] == null || this.registration[prop['fieldName']] == "") {
            isValid = false;
          }
        }

      }
    });


    return isValid;

  }

}



