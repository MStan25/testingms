import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RaceService} from '../../../core/components/race/race.service'
import { Observable } from 'rxjs/Rx';
import { NotificationService } from 'app/core/services/notification.service';

@Component({
  selector: 'app-race-modal',
  templateUrl: './race-modal.component.html',
  styleUrls: ['./race-modal.component.css'],
  providers: [RaceService]
})
export class RaceModalComponent implements OnInit {

  race: any;
  organiser: any;
  competitors: any;
  results: any;
  isModal: boolean = true;
  currentWindowWidth: number;
  
  constructor(
    public dialogRef: MatDialogRef<RaceModalComponent>,
    private service: RaceService,
    private notify: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.race = this.data['race'];
      this.organiser = this.data['organiser'];
    }
    

  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;

    this.fetchRaceCompetitors();
    this.fetchResults();
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;
  }

  fetchRaceCompetitors() {
    var filter = {};
    filter['id'] = this.race.eventId;
    Observable.from(this.service.getRaceCompetitors(filter)).subscribe( (result: any) =>{
      if(result['success'] === true){
        this.competitors = result['result'];
      } else {
      this.notify.error('Došlo je do greške!');
      }
    });
  }

  fetchResults() {
    var filter = {};
    filter['raceId'] = this.race.id;
    Observable.from(this.service.getResults(filter)).subscribe( (result: any) =>{
      if(result['success'] === true){
        this.results = result['result'];
      } else {
        this.notify.error('Došlo je do greške!');
      }
    });
  }
}
