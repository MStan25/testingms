import { Component, OnInit, ViewChild } from '@angular/core';
import { serializePath } from '@angular/router/src/url_tree';
import { debug } from 'util';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/from';
import { NotificationService } from '../core/services/notification.service';
import { UserService } from './user.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { Overlay } from '@angular/cdk/overlay';
declare var $: any;

@Component({
  selector: 'app-user-master',
  templateUrl: './user-master.component.html',
  providers: [UserService, NotificationService]
})
export class UserMasterComponent implements OnInit {
 


  constructor(private service: UserService, 
              public notify: NotificationService,
              public dialog: MatDialog,
              public overlay: Overlay
              ) {}
  

  ngOnInit() {
 
  }

  openModal(){
  //   const dialogRef = this.dialog.open( UserDetailModalComponent, {

  //     maxWidth: '100vw',
  //     maxHeight: '100vh',
  //     width: '95vw',
  //     height: 'auto',
  //     panelClass: 'custom-modalbox',
  //     data: {}
  // });
  // dialogRef.afterClosed().subscribe(() => {
  // });
  }

}
