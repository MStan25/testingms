import { Routes } from '@angular/router';

import { UserMasterComponent } from './user-master.component';

export const UserRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: UserMasterComponent
    }]
}];
