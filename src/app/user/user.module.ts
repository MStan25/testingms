
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { UserMasterComponent } from './user-master.component';
import { UserRoutes } from './user.routing';
import { MatSelectModule } from '@angular/material/select';
import { MaterialModule } from 'app/app.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserRoutes),
        FormsModule,
        SharedModule,
        MatSelectModule,
        MaterialModule
    ],
    declarations: [
        UserMasterComponent,
    ],
    entryComponents: [
        
    ]
})

export class UserModule { }
