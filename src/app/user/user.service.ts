import { Injectable, Component } from '@angular/core'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { MasterService } from '../core/services/master.service'

@Injectable()
 export class UserService {
    constructor(private service: MasterService) { }

    //DUMMY PRIMJER
    // filterUsers = function (filter, page, pagesize) {
    //     return this.service.promisePOST("api/user/fetch/"+page+"/"+pagesize, filter);
    // }

    getUserProfile = function (filter) {
        return this.service.niPostPromise("api/user/userprofile", filter);
    }
    
    getSports = function () {
        return this.service.niGetPromise("api/lookups/getsports");
    }
    getRecommendedRaces = function (filter) {
        return this.service.niPostPromise("api/races/filter/recommended", filter);
    }

    getOrganiser = function (filter) {
        return this.service.niPostPromise("api/races/organiser", filter);
    }

    getRaceCompetitors = function (filter) {
        return this.service.niPostPromise("api/races/getracecompetitors", filter);
    }

    getResults = function (filter) {
        return this.service.niPostPromise("api/races/filterraceresults", filter);
    }
    getUserRaces = function (filter) {
        return this.service.niPostPromise("api/races/filter/byuser", filter);
    }  
    saveUserProfile = function (profile) {
        return this.service.niPostPromise("api/user/saveuserprofile", profile);
    }
    

}