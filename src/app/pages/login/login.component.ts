import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AuthService } from '../../core/services/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { LoginService } from '../../core/services/login.service';
import { NotificationService } from '../../core/services/notification.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material';
import { RecoverModalComponent } from 'app/pages/login/recover-modal/recover-modal.component';
import { AuthService as ExternalAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html',
    providers: [AuthService, LoginService, NotificationService]
})

export class LoginComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node; 
    router: any;

    constructor(private element: ElementRef,
        private authService: AuthService,
        private externalAuthService: ExternalAuthService,
        private loginService: LoginService,
        private notify: NotificationService,
        private sanitizer: DomSanitizer,
        public dialog: MatDialog) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit() {
        this.email = "";
        this.password = "";
        setTimeout(() => {
            this.authService.logout();
        })


        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];

        this.loadImageData();
        this.loadImages();
    }

    imageFiles = [];

    loadImageData() {

        var that = this;

        var blob1 = null;
        var xhr1 = new XMLHttpRequest();
        var filename1 = "Tracey_login_bg_image_" + 1 + "A.jpg";
        xhr1.open("GET", "./assets/img/desktop/" + filename1);
        xhr1.responseType = "blob";
        xhr1.onload = function () {
            blob1 = xhr1.response;
            var file = new File([blob1], filename1, { type: 'image/jpeg', lastModified: Date.now() });
            that.imageFiles.push(file);
        }
        xhr1.send();


        var blob2 = null;
        var xhr2 = new XMLHttpRequest();
        var filename2 = "Tracey_login_bg_image_" + 2 + "A.jpg";
        xhr2.open("GET", "./assets/img/desktop/" + filename2);
        xhr2.responseType = "blob";
        xhr2.onload = function () {
            blob2 = xhr2.response;
            var file = new File([blob2], filename2, { type: 'image/jpeg', lastModified: Date.now() });
            that.imageFiles.push(file);
        }
        xhr2.send();


        var blob3 = null;
        var xhr3 = new XMLHttpRequest();
        var filename3 = "Tracey_login_bg_image_" + 3 + "A.jpg";
        xhr3.open("GET", "./assets/img/desktop/" + filename3);
        xhr3.responseType = "blob";
        xhr3.onload = function () {
            blob3 = xhr3.response;
            var file = new File([blob3], filename3, { type: 'image/jpeg', lastModified: Date.now() });
            that.imageFiles.push(file);
        }
        xhr3.send();

        var blob4 = null;
        var xhr4 = new XMLHttpRequest();
        var filename4 = "Tracey_login_bg_image_" + 4 + "A.jpg";
        xhr4.open("GET", "./assets/img/desktop/" + filename4);
        xhr4.responseType = "blob";
        xhr4.onload = function () {
            blob4 = xhr4.response;
            var file = new File([blob4], filename4, { type: 'image/jpeg', lastModified: Date.now() });
            that.imageFiles.push(file);
        }
        xhr4.send();

    }

    loadImages() {
        setInterval(() => {
            this.backgrounderOpacity = 1;
            var num = Math.floor(Math.random() * (4 - 1 + 1) + 1);

            setTimeout(() => {
                if (typeof this.imageFiles[num - 1] != undefined) {
                    var that = this;
                    var reader = new FileReader();
                    reader.readAsDataURL(this.imageFiles[num - 1]);
                    reader.onloadend = function () {
                        var base64data = reader.result;
                        that.setImageBackground(base64data);
                    }
                } else {
                  //  this.backgrounder = 'url("./assets/img/desktop/Tracey_login_bg_image_' + num + 'A.jpg")';
                }
         
            }, 800)
        }
            , 5000);

    }

    setImageBackground(base64data) {
      //  this.imgSrc = this.sanitizer.bypassSecurityTrustUrl(base64data);
        this.backgrounder = 'url('+ base64data +')';
    }

    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }


    signInWithGoogle(): void {
                this.isLoading = false;
                this.externalAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(result => {
                    console.log(result);
                    Observable.from (this.loginService.externallogin(result)).subscribe((user: any) => {
                        this.isLoading = false;
                        if (user.body.result == null) {
                            this.result['isError'] = 1;
                        } else {
                            localStorage.setItem('2DB9831C-7716-4043-9900-AA00D82B7F61', JSON.stringify(user.body.result));
                            this.loginService.setToken(true);
                            this.router.navigate(['dashboard']);
                        }
                    });
        
                });
            }

            signInWithFacebook(): void {
                        this.isLoading = false;
                        this.externalAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(result => {
                            console.log(result);
                            Observable.from(this.loginService.externallogin(result)).subscribe((user: any) => {
                                this.isLoading = false;
                                if (user.body.result == null) {
                                    this.result['isError'] = 1;
                                } else {
                                    localStorage.setItem('2DB9831C-7716-4043-9900-AA00D82B7F61', JSON.stringify(user.body.result));
                                    this.loginService.setToken(true);
                                    this.router.navigate(['dashboard']);
                                }
                            });
                
                        });
                    }



    public email: string = "";
    public password: string = "";
    backgrounder: any = 'url("./assets/img/desktop/Tracey_login_bg_image_1A.jpg")';
    imgSrc: any = {};
    backgrounderOpacity: any = 1;
    isLoading: boolean = false;
    newuser: any = {}
    isRegisterForm: boolean = false;
    result: any = {};
    login() {
        this.result = {};
        var cred = { email: this.email, password: this.password };
        this.authService.login(cred, this.result);

    }

    register() {

        this.isLoading = true;
        var cred = {};
        cred['email'] = this.newuser['Email'];
        cred['password'] = this.newuser['Password'];
        cred['firstname'] = this.newuser['FirstName'];
        cred['lastname'] = this.newuser['LastName'];
        cred['gender'] = 0;

        Observable.from(this.loginService.register(cred)).subscribe((result: any) => {
            this.isLoading = false;
            if (result != null && result != undefined && result['userId'] != null) {
                this.notify.success('Na Vašu email adresu je poslan verifikacijski link te Vas molimo da ga potvrdite.');
                //this.authService.login(cred, null); 
            } else {
                this.notify.error('Email je već zauzet');
            }
        });
    }

    displayRegister() {
        this.isRegisterForm = true;
    }
    displayLogin() {
        this.isRegisterForm = false;
    }


    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }



    
  openRecoverModal() {
    const dialogRef = this.dialog.open( RecoverModalComponent, {
        // scrollStrategy: this.overlay.scrollStrategies.noop(),
        panelClass: 'no-scroll',
        // width: '700px',
        // height: '90vh',
        maxWidth: '100vw',
        maxHeight: '100vh',
        width: '750px',
        height: '90vh',
        data: {  }
    });
    dialogRef.afterClosed().subscribe(() => {
      // this.fetchRaces();
    });
  }



}
