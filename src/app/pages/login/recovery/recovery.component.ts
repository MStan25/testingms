import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';

import { LoginService } from '../../../core/services/login.service';
@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.css'],
  providers: [LoginService, NotificationService]
})
export class RecoveryComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,
    private loginService: LoginService,
    private notify: NotificationService) {

  }

  userid: any = "";
  token: string = "";
  confirmPassword: string = "";
  newPassword: string = "";

  ngOnInit() {
    
    this.route.queryParams.subscribe((params: Params) => {
      this.userid = params["userid"];
      this.token = params["token"];
    });

  }

  changePassword() {
    if (!this.newPassword || this.newPassword == "" || !this.confirmPassword || this.confirmPassword == "") {
      this.notify.error("Lozinka ne smije biti prazna!");
      return;
    }

    if (this.newPassword != this.confirmPassword) {
      this.notify.error("Lozinke se nepodudaraju!");
      return;
    }

    var request = {
      "userId": this.userid,
      "token": this.token,
      "password": this.newPassword,
      "verifypassword": this.confirmPassword

    };




    Observable.from(this.loginService.accountRecovery(request)).subscribe((result: any) => {
      if (result != null) {
        if (result == true) {
          this.notify.success("Lozinka je uspiješno promijenjena! Molimo prijavite se za nastavak");
          this.router.navigate(['login']);
        }
        else {
          this.notify.error("Verifikacijski link je istekao!");
        }
      }
    });


  }




}
