
import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs/Rx';

import { NotificationService } from '../../../core/services/notification.service';
import { LoginService } from '../../../core/services/login.service';

@Component({
  selector: 'app-recover-modal',
  templateUrl: './recover-modal.component.html',
  styleUrls: ['./recover-modal.component.css'],
  providers: [LoginService,NotificationService]
})
export class RecoverModalComponent implements OnInit {

  currentWindowWidth: number;
  recoverEmail: any = "";
  constructor(
    public dialogRef: MatDialogRef<RecoverModalComponent>,
    private loginService: LoginService,
    private notify: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;
  }

  recoverAccount() {

    var request = { "email": this.recoverEmail };

    Observable.from(this.loginService.recoverAccount(request)).subscribe((result: any) => {
      if (result != null) {
        if(result == true){
          this.notify.success("Link za oporavak računa poslan je na " + this.recoverEmail);
          this.dialogRef.close();
        }
        else{
          this.notify.error("Email adresa ne postoji");
          this.dialogRef.close();
        }
      }
    });
  }
}



