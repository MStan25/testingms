

import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { Params } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';

import { LoginService } from '../../../core/services/login.service';
@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css'],
  providers: [LoginService, NotificationService]
})
export class VerificationComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,
    private loginService: LoginService,
    private notify: NotificationService) {

  }

  userid: any = "";
  token: string = "";
  confirmPassword: string = "";
  newPassword: string = "";

  ngOnInit() {

    this.route.queryParams.subscribe((params: Params) => {
      this.userid = params["userid"];
      this.token = params["token"];
      this.verify();
    });

  }

  verify() {
    var request = {
      "userId": this.userid,
      "token": this.token

    };




    Observable.from(this.loginService.accountVerification(request)).subscribe((result: any) => {
      if (result != null) {
        debugger;
        if (result == true) {
          this.notify.success("Email verificiran! Molimo prijavite se za nastavak");
          this.router.navigate(['login']);
        }
        else {
          this.notify.error("Verifikacijski link je istekao!");
        }
      }
    });


  }




}
