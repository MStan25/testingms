import { Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { PricingComponent } from './pricing/pricing.component';
import { LockComponent } from './lock/lock.component';
import { LoginComponent } from './login/login.component';
import { RecoveryComponent } from './login/recovery/recovery.component';
import { VerificationComponent } from 'app/pages/login/verification/verification.component';

export const PagesRoutes: Routes = [

    {
        path: '',
        children: [ {
            path: '',
            component: LoginComponent
        }, {
            path: 'lock',
            component: LockComponent
        }, {
            path: 'register',
            component: RegisterComponent
        }, {
            path: 'pricing',
            component: PricingComponent
        }
        , {
            path: 'userpasswordrecovery',
            component: RecoveryComponent
        },{
            path: 'verifyuser',
            component: VerificationComponent
        },
    ]
    }
];
