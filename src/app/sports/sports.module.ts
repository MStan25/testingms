import { NgModule } from '@angular/core';
import { SportsRoutes } from './sports.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../core/components/shared.module'
import { SportsComponent } from './sports.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SportsRoutes),
    FormsModule,
    SharedModule
  ],
  declarations: [SportsComponent]
})
export class SportsModule { }

