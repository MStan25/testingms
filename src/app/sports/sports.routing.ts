import { Routes, RouterModule } from '@angular/router';
import { SportsComponent } from './sports.component';

export const SportsRoutes: Routes = [{
    path: '',
    children: [{
        path: '',
        component: SportsComponent
    }]
}];
