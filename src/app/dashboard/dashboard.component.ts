

import { Component, OnInit, ViewChild } from '@angular/core';
import { serializePath } from '@angular/router/src/url_tree';
import { debug } from 'util';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/from';
import { NotificationService } from '../core/services/notification.service';
import { DashboardService } from './dashboard.service';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { RaceModalComponent } from '../races/detail/race-modal/race-modal.component';
import { disableBindings } from '@angular/core/src/render3';
import { Overlay } from '@angular/cdk/overlay';

declare var $: any;


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService, NotificationService]
})
export class DashboardComponent implements OnInit {
  months = [
    {value: "0", viewValue: 'Svi mjeseci'},
    {value: "1", viewValue: 'Siječanj'},
    {value: "2", viewValue: 'Veljača'},
    {value: "3", viewValue: 'Ožujak'},
    {value: "4", viewValue: 'Travanj'},
    {value: "5", viewValue: 'Svibanj'},
    {value: "6", viewValue: 'Lipanj'},
    {value: "7", viewValue: 'Srpanj'},
    {value: "8", viewValue: 'Kolovoz'},
    {value: "9", viewValue: 'Rujan'},
    {value: "10", viewValue: 'Listopad'},
    {value: "11", viewValue: 'Studeni'},
    {value: "12", viewValue: 'Prosinac'},
  ];

  applications = [
    {value: 0, viewValue: 'Otvorene prijave'},
    {value: 1, viewValue: 'Zatvorene prijave'},
    {value: 2, viewValue: 'Moje prijave'},
  ];

  sports = [
    {value: 0, viewValue: 'Moji sportovi'},
    {value: 1, viewValue: 'Svi sportovi'},
  ];
  filterApplication: any = 0;
  filterSport: any = 0;
  filterMonth: any = 1;
  totalCount: any = -1;
  userid: any = '';
  firstName: any = '';
  lastName: any = '';
  email: String = '';
  isProfileLoading: boolean = false;
  isMedalsLoading: boolean = false;
  isUpcommingRacesLoading: boolean = false;
  isFinishedRacesLoading: boolean = false;
  isAwardsLoading: boolean = false;
  isProfileSaving: boolean = false;
  isNewsAdmin: boolean = false;
  isNewsAdminView: boolean = false;
  isNewsEditing: boolean = false;
  isWelcomeTextVisible: boolean = false;
  editingNewsID: string = '';
  profile: any = {};
  welcomeMessage: any = '';
  welcomeWidgetText: any = '';
  addedNewsStatusText: any = '';

//   @ViewChild('paging') pagingComponent: PagingComponent;

  constructor(private service: DashboardService, 
              public notify: NotificationService,
              public dialog: MatDialog,
              public overlay: Overlay
              ) {}
  
  recommendedRaces: any = [];
  race: any = [];  
  latestNews: any = [];
  news: any = [];
  // sports: any = [];
  ngOnInit() {
   this.fetchRaces(0);   
   this.fetchLatestNews();   
  //  this.loadOrganiser();
   this.getUserProfile();   
   this.getUserWelcomeMessage();   
   this.getWelcomeWidgetText(); 
   this.checkPastAccess();
   this.checkNewsAdmin();
  }

  fetchRaces(month: any) {
    var filter = {};
    filter['upcoming_month'] = month; 
    // @FieldAnnotation(fieldName = "race_start")
    // private Date raceStart;
    // @Expose
    // @FieldAnnotation(fieldName = "race_end")
    // private Date raceEnd;
    // @Expose
    // @FieldAnnotation(fieldName = "max_participants")
    // private Integer maxParticipants;
    Observable.from(this.service.getRecommendedRaces(filter)).subscribe( (result: any) =>{
      // debugger;
      if(result['success'] == true){
        this.recommendedRaces = result['result'];
        //this.totalCount = result['totalCount']; //JM
      }
      // if(result != undefined){
      //   result.forEach(element => {
      //     this.sports.push(element.name);
      //   });
      // }
    });
  }


  fetchLatestNews() {
    var filter = {};
    Observable.from(this.service.getLatestNews(filter)).subscribe( (result: any) =>{
      // debugger;
      if(result['success'] == true){
        this.latestNews = result['result'];
      }
    });
  }

  addNews(news: string) {
    var filter = {};
    filter['news'] = news;
    this.addedNewsStatusText = "";
    Observable.from(this.service.addNews(filter)).subscribe((result: any) => {
      if (result != null) {
        this.addedNewsStatusText = "Saved";        
      }
      this.fetchLatestNews();       
      //location.reload();
      this.latestNews.push();
    });
  }

  deleteNews(newsid: string, news: string) {
    if(confirm("Potvrdite brisanje obavijesti: " + news)) {
      var filter = {};
      filter['id'] = newsid;
      Observable.from(this.service.deleteNews(filter)).subscribe((result: any) => {
        /*
        if(result['success'] == true){
        }
        */
        this.fetchLatestNews();       
        this.latestNews.push();
      });
    }    
  }

  editNews(newsid: string) {
    this.isNewsEditing = true;
    this.editingNewsID = newsid;
  }

  updateNews(newsid: string, news: string) {
    var filter = {};    
    this.isNewsEditing = false;
    this.editingNewsID = "";
    filter['id'] = newsid;    
    filter['updatednews'] = news;
    Observable.from(this.service.updateNews(filter)).subscribe((result: any) => {     
      //if (result != null){ 
      //}
      this.fetchLatestNews();       
      //location.reload();
      this.latestNews.push();
    });
  }

  cancelEditNews(newsid: string, news: string) {
    this.isNewsEditing = false;
    this.editingNewsID = "";
  }

  toggleNewsAdminView() {
    this.isNewsAdminView = !this.isNewsAdminView;
  }

  showNewsEditBox(newsid: string): boolean {
    
    if (!this.isNewsAdmin) {
      return false;
    }

    if (!this.isNewsAdminView) {
      return false;
    }

    if (newsid == this.editingNewsID) {
      return true;
    }

    return false;

  }

  getUserProfile() {
    this.isProfileLoading = true;
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
    var request = {};
    this.userid = session["id"];
    request["userId"] = session["id"];
    Observable.from(this.service.getUserProfile(request)).subscribe((result: any) => {
      if (result != null) {
        this.profile = result;
        this.isProfileLoading = false;
        this.firstName = result['firstname'];
        this.lastName = result['lastname'];
        this.email = result['email'];    
        /*    
        if (this.email.indexOf("@stotinka.hr") > 0) {
          this.isNewsAdmin = true;
          this.isNewsAdminView = true;
        } 
        else {
          this.isNewsAdmin = false;
          this.isNewsAdminView = false;
        } 
        */
      }
    });
  }

  getUserWelcomeMessage() {    
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
    var request = {};
    request["userId"] = session["id"];
    Observable.from(this.service.getUserWelcomeMessage(request)).subscribe((result: any) => {
      if (result != null) {
        this.welcomeMessage = result;
      }
    });
  }

  getWelcomeWidgetText() {    
    var filter = {};
    filter['code'] = "WDG_WELCOME";
    filter['type'] = "TEXT"; 
    this.welcomeWidgetText = "";
    Observable.from(this.service.getWebContent(filter)).subscribe((result: any) => {
      if (result != null) {
        this.welcomeWidgetText = result;
      }
    });
  }

  checkPastAccess() {    
    var filter = {};
    filter['user_id'] = this.userid;
    filter['current_date_offset'] = "-15"; 
    Observable.from(this.service.checkPastAccess(filter)).subscribe((result: any) => {
      if (result == "YES") {
          this.isWelcomeTextVisible = false;
      } else {
          this.isWelcomeTextVisible = true;
      }         
    });
  }

  checkNewsAdmin() {    
    var filter = {};
    filter['user_id'] = this.userid;
    filter['role_name'] = "NEWS_ADMIN"; 
    Observable.from(this.service.isRoleMember(filter)).subscribe((result: any) => {
      if (result == "YES") {
          this.isNewsAdmin = true;
          this.isNewsAdminView = true;
      } else {
          this.isNewsAdmin = false;
          this.isNewsAdminView = false;
      }         
    });
  }


  /** Modals */
  organiser: any = {};
  loadOrganiser(race) {
    var filter = {};
    filter["id"] = race.organiserId;
    Observable.from(this.service.getOrganiser(filter)).subscribe((result: any) => {
      if (result != null) {
        this.organiser = result;
        if(result['imageMedium'] != null){
          
          this.organiser["imageLogo"] = "https://www.stotinka.hr/img/" +  result['imageMedium'];
        }
        this.openRaceModal(race, this.organiser);
      }
    });
  }

  openRaceModal(race, organiser) {
    // const dialogRef = this.dialog.open( RaceModalComponent, {
    //     // scrollStrategy: this.overlay.scrollStrategies.noop(),
    //     panelClass: 'no-scroll',
    //     // width: '700px',
    //     // height: '90vh',
    //     maxWidth: '100vw',
    //     maxHeight: '100vh',
    //     width: '750px',
    //     height: '100vh',
    //     data: { race, organiser }
    // });
    // dialogRef.afterClosed().subscribe(() => {
    //   // this.fetchRaces();
    // });
  }
}
