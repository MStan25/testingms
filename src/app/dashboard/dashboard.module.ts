import { NgModule } from '@angular/core';
import { DashboardRoutes } from './dashboard.routing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { RaceModalComponent } from '../races/detail/race-modal/race-modal.component';
// import { RaceComponent } from '../core/components/race/race.component'
import { MaterialModule } from 'app/app.module';
import { SharedModule } from 'app/core/components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    FormsModule,
    MaterialModule,
    SharedModule
  ],
  declarations: [DashboardComponent, RaceModalComponent],
  entryComponents: [
    RaceModalComponent
  ]
})
export class DashboardModule { }


