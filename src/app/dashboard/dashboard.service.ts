import { Injectable, Component } from '@angular/core'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { MasterService } from '../core/services/master.service'

@Injectable()
 export class DashboardService {
    constructor(private service: MasterService) { }

    getSports = function () {
        return this.service.niGetPromise("api/lookups/getsports");
    }
    getRecommendedRaces = function (filter) {
        return this.service.niPostPromise("api/races/filter/recommended", filter);
    }

    getOrganiser = function (filter) {
        return this.service.niPostPromise("api/races/organiser", filter);
    }

    getRaceCompetitors = function (filter) {
        return this.service.niPostPromise("api/races/getracecompetitors", filter);
    }

    getResults = function (filter) {
        return this.service.niPostPromise("api/races/filterraceresults", filter);
    }

    getUserProfile = function (filter) {
        return this.service.niPostPromise("api/user/userprofile", filter);
    }

    getUserWelcomeMessage = function (filter) {
        return this.service.niPostPromise("api/user/userwelcomemessage", filter);
    }

    isRoleMember = function (filter) {
        return this.service.niPostPromise("api/user/isrolemember", filter);
    }
    
    checkPastAccess = function (filter) {
        return this.service.niPostPromise("api/user/checkpastaccess", filter);
    }

    getLatestNews = function (filter) {
        return this.service.niPostPromise("api/general/latestnews", filter);
    }

    addNews = function (filter) {
        return this.service.niPostPromise("api/general/addnews", filter);
    }

    deleteNews = function (filter) {
        return this.service.niPostPromise("api/general/deletenews", filter);
    }
    
    updateNews = function (filter) {
        return this.service.niPostPromise("api/general/updatenews", filter);
    }

    getWebContent = function (filter) {
        return this.service.niPostPromise("api/general/getwebcontent", filter);
    }

}