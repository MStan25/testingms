import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (localStorage.getItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E')) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
      //  return true;
    }
}
