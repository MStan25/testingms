import { Injectable } from '@angular/core';
import {Injector} from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpHeaders,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private inj: Injector) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        var user: any;
        
        if (localStorage.getItem('login_temp_data')) {
            user = JSON.parse(localStorage.getItem("login_temp_data"))
            localStorage.removeItem('login_temp_data')
        } else if (localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61')) {
            user = JSON.parse(localStorage.getItem("2DB9831C-7716-4043-9900-AA00D82B7F61"));
            user["email"] = user['tokenId'];
            user["password"] = user["password"];
        } else {
            user["email"] = "";
            user["password"] = "";
        }

        var cors = new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'q=0.8;application/json;q=0.9',
            'Authorization': 'Basic ' + btoa(user["email"] + ':' + user["password"]),
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PATCH, DELETE',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            'Access-Control-Allow-Credentials': 'true',
        });

        var changedReq = req.clone({ headers: cors });
        return next.handle(changedReq);
    }
}