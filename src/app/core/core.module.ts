import { forwardRef, NgModule, ModuleWithProviders } from '@angular/core';
import { NotificationService } from './services/notification.service'
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { MasterService } from './services/master.service';
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [

    CommonModule
  ],
  //  providers: [forwardRef(() => AuthService)],


})
export class CoreModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        AuthGuard,
        NotificationService,
        AuthService,
        MasterService

      ],
    };
  }


}
