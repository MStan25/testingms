import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { NotificationService } from 'app/core/services/notification.service';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { RegisterModalComponent } from 'app/races/register-modal/register-modal.component';
import { RaceService } from '../race.service';

@Component({
  selector: 'app-race-detail',
  templateUrl: './race-detail.component.html',
  styleUrls: ['./race-detail.component.css']
})
export class RaceDetailComponent implements OnInit {

  public currentWindowWidth: number;

  @Input() race: any;
  @Input() isUserRace: any;
  @Input() codebooks: any;
  @Output() setFormDetailsEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  isModal: boolean = false;
  isTableLoading: boolean = false;

  competitors: any;
  results: any;

  constructor(private service: RaceService, private notify: NotificationService,public dialog: MatDialog) { }

  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;

    this.loadOrganiser();
    this.loadRaceRegistrationDetail();
    this.fetchRaceCompetitors();
    this.fetchResults();
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;
  }

  organiser: any = {};
  loadOrganiser() {
    var filter = {};
    filter["id"] = this.race.organiserId;
    Observable.from(this.service.getOrganiser(filter)).subscribe((result: any) => {
      if (result != null) {
        this.organiser = result;
        if (result['imageMedium'] != null) {

          this.organiser["imageLogo"] = "https://www.stotinka.hr/img/" + result['imageMedium'];
        }
      }
    });
  }

  totalPaid: any = 0;
  totalHasNumber: any = 0;
  totalRegistered: any = 0;

  loadRaceRegistrationDetail() {
    var filter = {};
    filter["id"] = this.race.id;
    Observable.from(this.service.getRaceRegistrationDetail(filter)).subscribe((result: any) => {
      if (result != null) {

        var stats = result;

        stats.forEach(element => {
          if (element.name == 'totalPaid') {
            this.totalPaid = element.intValue1;
          } else if (element.name == 'totalHasNumber') {
            this.totalHasNumber = element.intValue1;
          } else if (element.name == 'totalRegistered') {
            this.totalRegistered = element.intValue1;
          }
        });
      }
    });
  }

  competitorsByCountries: any = [];

  loadRaceRegistrationCountry() {
    var filter = {};
    filter["id"] = 442;
    Observable.from(this.service.getRaceRegistrationCountry(filter)).subscribe((result: any) => {
      if (result != null) {
        this.competitorsByCountries = result;
      }
    });
  }

  closeDetails() {
    this.setFormDetailsEvent.emit(false);
  }

  // registerToRace() {

  //   debugger;
  //   var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));

  //   var request = {};
  //   request["RaceId"] = this.race["id"];
  //   request["UserId"] = session["id"];
  //   request["Gender"] = 1;
  //   Observable.from(this.service.registerToRace(request)).subscribe((result: any) => {
  //     if (result != null) {
  //       this.notify.success("Korisnik je uspiješno registriran na utrku!");
  //     }
  //   });
  // }


  openRegisterToRace() {
    const dialogRef = this.dialog.open( RegisterModalComponent, {
        // scrollStrategy: this.overlay.scrollStrategies.noop(),
       
        // width: '700px',
        // height: '90vh',
        maxWidth: '100vw',
        maxHeight: '100vh',
        width: '60vw',
        height: 'auto',
        panelClass: 'custom-modalbox',
        data: { "id": this.race.id, "codebooks":this.codebooks }
    });
    dialogRef.afterClosed().subscribe(() => {
      // this.fetchRaces();
    });
  }

  fetchRaceCompetitors() {
    var filter = {};
    filter['id'] = this.race.eventId;
    Observable.from(this.service.getRaceCompetitors(filter)).subscribe( (result: any) =>{
      if(result['success'] === true){
        this.competitors = result['result'];
      }
      //TODO
      // if(result['result'].length === 0 ){
      //   this.competitors = result['result'];
      // }
    });
  }

  fetchResults() {
    var filter = {};
    filter['raceId'] = this.race.id;
    Observable.from(this.service.getResults(filter)).subscribe( (result: any) =>{
      if(result['success'] === true){
        this.results = result['result'];
      }
    });
  }
}
