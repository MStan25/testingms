import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'app-race-detail-part',
  templateUrl: './race-detail-part.component.html',
  styleUrls: ['./race-detail-part.component.css']
})
export class RaceDetailPartComponent implements OnInit {

  public currentWindowWidth: number;
  @Input() isModal: boolean;
  @Input() race: any;
  @Input() organiser: any;
  @Input() competitors: any;
  @Input() results: any;


  constructor() { }

  ngOnInit() {
    this.currentWindowWidth = window.innerWidth;
  }

  @HostListener('window:resize')
  onResize() {
    this.currentWindowWidth = window.innerWidth;
  }
}
