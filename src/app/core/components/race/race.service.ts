
import { Injectable, Component } from '@angular/core'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { MasterService } from '../../services/master.service'
@Injectable()
export class RaceService {
    constructor(private service: MasterService) { }

    getRaces = function (filter) {
        return this.service.niPostPromise("api/races/filter/racesbystatus", filter);
    }
    getOrganiser = function (filter) {
        return this.service.niPostPromise("api/races/organiser", filter);
    }
    getRaceRegistrationDetail = function (filter) {
        return this.service.niPostPromise("api/races/raceregistrationdetail", filter);
    }
    getRaceRegistrationCountry = function (filter) {
        return this.service.niPostPromise("api/races/raceregistrationcountry", filter);
    }

    registerToRace = function (filter) {
        return this.service.niPostPromise("api/races/registertorace", filter);
    }

    getRaceRegistrationComponents = function (filter) {
        return this.service.niPostPromise("api/formparameterization/getbyraceregistration", filter);
    }

    getUserProfile = function (filter) {
        return this.service.niPostPromise("api/user/userprofile", filter);
    }

    getRaceCompetitors = function (filter) {
        return this.service.niPostPromise("api/races/getracecompetitors", filter);
    }

    getResults = function (filter) {
        return this.service.niPostPromise("api/races/filterraceresults", filter);
    }
}