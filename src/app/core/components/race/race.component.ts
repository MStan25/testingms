import { NotificationService } from './../../services/notification.service';
import { Component, OnInit, Input, HostListener } from '@angular/core';
import { CodebookService } from 'app/core/services/codebook.service';
import { AuthService } from 'app/core/services/auth.service';
import { Observable } from 'rxjs';
import { RaceService } from './race.service';


@Component({
  selector: 'app-race',
  templateUrl: './race.component.html',
  styleUrls: ['./race.component.css']
})
export class RaceComponent implements OnInit {

    @Input() statusFilter: string;
  
    isContentLoaded: boolean = false;
    dropdowns: any;
    isFormDetails: boolean;
    isUserRace: boolean;
  
    size: number;
    user: any = {};
    filter: any = {};
    raceFilter: any = {};
    pages: any = {};
  
    races: any = [];
    race: any = {};
  
    isLoading = false;
  
    constructor(
      private raceService: RaceService,
      public codebookservice: CodebookService,
      public authService: AuthService,
      public notify: NotificationService) { }
  
    ngOnInit() {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.checkIfUserRace();
      this.fetchCodebooks();
    }
  
    checkIfUserRace() {
      if ( this.statusFilter == "recommended") {
        this.isUserRace = false;
      } else {
        this.isUserRace = true;
      }
    }

    fetchRaces() {
      this.isLoading = true;
  
      this.raceFilter['userId'] = this.user['id'];
      this.raceFilter['statusFilter'] = this.statusFilter;
  
      this.filter['filter'] = this.raceFilter;
      console.log(this.filter);
      Observable.from(this.raceService.getRaces(this.filter)).subscribe((result: any) => {
        if (result['success'] === true) {
          this.races = result['result'];
          this.size = result['totalCount'];
        } else {
          this.notify.error('Došlo je do greške!');
        }
        this.isLoading = false;
      });
    }
  
    fetchCodebooks(){
      Observable.forkJoin([
        this.codebookservice.getGenders(), 
        this.codebookservice.getSports(), 
        this.codebookservice.getClubs(), 
        this.codebookservice.getSize(), 
        this.codebookservice.getCountries(),  
        this.codebookservice.getMeals(),   
        this.codebookservice.getTransport(),
      ])
        .subscribe((response) => {
          
          // response[0]['result'].forEach(function (item) { item['itemValue'] = item['countryCode']; item['itemName'] = item['name'] });
  
            this.dropdowns = {
                genders: response[0],
                sports: response[1],
                clubs: response[2],
                size: response[3],
                countries: response[4],
                meals: response[5],
                transport: response[6]
            };
            this.isContentLoaded = true;
        });
    }
  
    setFormDetails($event) {
      this.isFormDetails = $event;
    }
  
    onSelect(race) {
      this.race = race;
      this.isFormDetails = true;
    }
  
    /** Pagination */
    onDisplayData(pagination: any) {
      this.pages['pageSize'] = pagination['pageSize'];
      this.pages['page'] = pagination['page'];
      this.filter['pages'] = this.pages;
      this.fetchRaces();
    }
  
  }
