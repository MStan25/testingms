import { Component, OnInit, Input, HostListener, EventEmitter } from '@angular/core';
import { Output } from '@angular/core';

@Component({
  selector: 'ni-datedropdown',
  templateUrl: './datedropdown.component.html',
  styleUrls: ['./datedropdown.component.css']
})
export class DatedropdownComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.loadDates();
  }

  @Input() selectedDate: any;
  @Input() isMandatory: boolean = false;
  @Output() validation: EventEmitter<any> = new EventEmitter<any>();
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

  days: any = [];
  years: any = [];
  months: any = [
    { Id: 0, Value: "Siječanj" },
    { Id: 1, Value: "Veljača" },
    { Id: 2, Value: "Ožujak" },
    { Id: 3, Value: "Travanj" },
    { Id: 4, Value: "Svibanj" },
    { Id: 5, Value: "Lipanj" },
    { Id: 6, Value: "Srpanj" },
    { Id: 7, Value: "Kolovoz" },
    { Id: 8, Value: "Rujan" },
    { Id: 9, Value: "Listopad" },
    { Id: 10, Value: "Studeni" },
    { Id: 11, Value: "Prosinac" }

  ];

  isDateValid: boolean = false;
  selectedDay: any;
  selectedMonth: any;
  selectedYear: any;

  loadDates() {

    var that = this;
    setTimeout(function () {
      if (typeof that.selectedDate == 'string') {
        var date = new Date(<string>that.selectedDate);
        that.selectedDate = date;
      }

      that.setDMY();
      that.loadYears();
      that.loadDays();
    }, 500);


  }

  loadYears() {

    if (this.years != null && this.years.length != 0) {
      this.years = this.years;
    } else {
      var currentYear = (new Date()).getFullYear();
      for (var i = currentYear; i >= currentYear - 100; i--) {
        this.years.push(i);
      }
    }

  }

  loadDays() {
    this.days = [];
    var daysize = this.getDaysInMonth(this.selectedMonth + 1, this.selectedYear)
    for (var i = 1; i <= daysize; i++) {
      this.days.push(i);
    }
    this.onDayChange();
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  onYearMonthChange() {

    this.selectedDay = 1;
    this.loadDays();
  }

  onDayChange() {
    if (this.selectedYear != null && this.selectedMonth != null && this.selectedDay != null) {
      var date = new Date(this.selectedYear, this.selectedMonth, this.selectedDay);
      this.selectedDate = date;
    }
    else
      this.selectedDate = null;
    this.validate();

    if (this.selectedDate != null)
      this.setDMY();
  }

  setDMY() {
    if (this.selectedDate != null) {
      this.selectedDay = this.selectedDate.getDate();
      this.selectedMonth = this.selectedDate.getMonth();
      this.selectedYear = this.selectedDate.getFullYear();
    }
  }

  validate() {
    if (this.isMandatory) {
      if (this.selectedDate != null)
        this.isDateValid = true;
      else
        this.isDateValid = false;
    } else {
      this.isDateValid = true;
    }


    if (this.validation != null) {
      this.validation.emit(this.isDateValid);
    }
    if (this.valueChange != null) {
      this.valueChange.emit(this.selectedDate);
    }

  }

}
