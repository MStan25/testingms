import { RaceDetailComponent } from './race/race-detail/race-detail.component';
import { CodebookService } from './../services/codebook.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MatSelectModule, MatTooltipModule } from '@angular/material';
import { PaginationComponent } from './pagination/pagination.component';
import { RaceService } from 'app/core/components/race/race.service';
import { RaceComponent } from 'app/core/components/race/race.component'
import { MaterialModule } from 'app/app.module';
import { RaceDetailPartComponent } from './race/race-detail/race-detail-part/race-detail-part.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        MatSelectModule,
        MatTooltipModule,
        MaterialModule
    ],
    declarations: [
        PaginationComponent,
        //DatedropdownComponent, 
        //UserComponent,
        RaceComponent,
        RaceDetailComponent,
        RaceDetailPartComponent
    ],
    exports: [
        PaginationComponent,
        // DatedropdownComponent,
        //UserComponent,
        RaceComponent,
        RaceDetailComponent,
        RaceDetailPartComponent

    ],
    entryComponents: [
        
    ],
    providers: [RaceService, CodebookService]
})

export class SharedModule { }
