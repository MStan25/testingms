import { Component, Input, OnInit, Output, OnChanges, EventEmitter } from '@angular/core';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css'],
    providers: []
})
export class PaginationComponent {
    @Input() size: number;
    @Output() displayDataEvent: EventEmitter<any> = new EventEmitter<any>();


    ngOnInit() {
        this.currentPage = 1;
        this.pageSize = 10;
        this.maxPageNumbers = 7;
        this.setPages(this.size);
        this.navigate(1);
    }

    ngOnChanges() {
        this.setPages(this.size);
        this.navigate(1);
    }

    currentPage: number;
    maxPage: number;
    dataSize: number;
    pageSizes = [10, 20, 50, 100];
    pageSize: number;
    pages: number[];
    maxPageNumbers: number;

    pagination: any;

    navigateBack() {
        if (this.currentPage == null || this.currentPage == undefined ) {
            this.currentPage = 1;
            this.navigate(this.currentPage);
            return;
        }
        if (this.currentPage > 1)
            this.navigate(this.currentPage - 1);
    }

    navigateForward() {
        if (this.currentPage == null) {
            this.currentPage = 1;
            this.navigate(this.currentPage);
            return;
        }
        if (this.currentPage < this.maxPage)
            this.navigate(this.currentPage + 1);
    }

    navigateStart() {
        this.navigate(1);
    }

    navigateEnd() {
        this.navigate(this.maxPage);
    }

    setPages(size) {
        this.dataSize = size;
        if (this.dataSize == 0) {
            this.currentPage = 1;
            this.maxPage = 1;
        } else {
            if (this.dataSize % this.pageSize == 0)
                this.maxPage = this.dataSize / this.pageSize;
            else
                this.maxPage = Math.floor(this.dataSize / this.pageSize) + 1;
        }
        this.pages = [];

        let i;

        if (this.maxPage <= this.maxPageNumbers || this.currentPage <= Math.ceil(this.maxPageNumbers / 2)) {
            i = 1;
        } else if (this.currentPage > this.maxPage - Math.floor(this.maxPageNumbers / 2)) {
            i = this.maxPage - this.maxPageNumbers + 1;
        } else {
            i = this.currentPage - Math.floor(this.maxPageNumbers / 2);
        }

        for (let j = 0; i <= this.maxPage && j < this.maxPageNumbers; i++) {
            this.pages[j] = i;
            ++j;
        }
    }

    navigate(page: number) {
        if (page == undefined || page == null)
            page = 1;

        this.currentPage = page;
        this.setPages(this.size);

        this.pagination = {
            "page": this.currentPage,
            "pageSize": this.pageSize
        }

       this.displayDataEvent.emit(this.pagination);
    }

    onPageSizeChange(event) {
        this.setPages(this.size);
        this.navigate(1);
    }

    enterPage() {
        this.navigate(this.currentPage);
    }
}