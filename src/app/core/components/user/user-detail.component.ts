import { Component, OnInit, ViewChild } from '@angular/core';
import { serializePath } from '@angular/router/src/url_tree';
import { debug } from 'util';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/from';
import { NotificationService } from '../../services/notification.service';
import { UserService } from '../../../user/user.service';
import * as moment from 'moment';
import * as $ from 'jquery';
import { DatedropdownComponent } from '../datedropdown/datedropdown.component';

//import { PagingComponent } from 'app/core/components/paging.component';



@Component({
  selector: 'app-user-component',
  templateUrl: './user-detail.component.html',
  providers: [UserService, NotificationService]
})
export class UserComponent implements OnInit {

  //   @ViewChild('paging') pagingComponent: PagingComponent;
 
  isProfileLoading: boolean = false;
  isMedalsLoading: boolean = false;
  isUpcommingRacesLoading: boolean = false;
  isFinishedRacesLoading: boolean = false;
  isAwardsLoading: boolean = false;
  isProfileSaving: boolean = false;
  profile: any = {};

  finishedRaces: any = [];

  codebooks: any = {};
  constructor(private service: UserService, public notify: NotificationService) {

  }

  ngOnInit() {
    var left = document.getElementById('leftdiv').offsetHeight;
    var left90 = (left * 0.90);

    setTimeout(() => {
      var tabs = document.getElementsByClassName('scroll');
      for (var i = 0; i < tabs.length; i++) {
        document.getElementById(tabs[i].id).style.height = left90.toString() + "px";
      }

    }, 500);

    document.getElementById('rightdiv').style.height = left.toString() + "px";
    this.getUserProfile();
  }



  getUserProfile() {
    this.isProfileLoading = true;
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
    var request = {};
    request["userId"] = session["id"];
    Observable.from(this.service.getUserProfile(request)).subscribe((result: any) => {
      if (result != null) {
        this.profile = result;
        this.isProfileLoading = false;
      }
    });
  }

  getUserMedals() {
    this.isMedalsLoading = true;
    setTimeout(() => {
      this.isMedalsLoading = false;
    }, 1500);
  }

  getUserUpcommingRaces() {

    this.isUpcommingRacesLoading = true;
    setTimeout(() => {
      this.isUpcommingRacesLoading = false;
    }, 1000);
  }

  getUserFinishedRaces() {
    this.isFinishedRacesLoading = true;
    var session = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
    var request = {};
    request["userId"] = session["id"];
    Observable.from(this.service.getUserRaces(request)).subscribe((result: any) => {
      this.isFinishedRacesLoading = false;
      if (result != null) {
        this.finishedRaces = result['result'];
      }
    });



  }

  getUserAwards() {
    this.isAwardsLoading = true;
    setTimeout(() => {
      this.isAwardsLoading = false;
    }, 1000);
  }


  saveProfile() {
    this.isProfileSaving = true;
    Observable.from(this.service.saveUserProfile(this.profile)).subscribe((result: any) => {
      this.isProfileSaving = false;
      if (result != null && result == true) {
        this.notify.success("Profil je ažuriran!");
      } else {
        this.notify.error("Došlo je do greške!");

      }
      this.isProfileSaving = false;
    });
  }




}
