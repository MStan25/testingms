import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { LoginService } from '../services/login.service';
import { NotificationService } from './notification.service';

@Injectable()
export class AuthService {
    constructor(private loginService:LoginService, private router: Router) { }
    
    currentUser: any;

    login(UserData: any, result: any) {
        Observable.from(this.loginService.login(UserData)).subscribe((user: any) => {
            // debugger;
            if (user.tokenId == undefined || user.tokenId == null) {
                result['isError'] = 1;
            } else {
                localStorage.setItem('user', JSON.stringify(user));
                localStorage.setItem('2DB9831C-7716-4043-9900-AA00D82B7F61', JSON.stringify(user));
                localStorage.setItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E', JSON.stringify(user));
                this.loginService.setToken(true);
                this.router.navigate(['dashboard']);
            }
        });
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user_info'));
    }

    logout() {
        this.loginService.logout();
        this.loginService.setToken(false);
        this.router.navigate(['login']);
    }

    isLogged(): boolean {
        if (localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61') != null && localStorage.getItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E'))
            return true;
        else
            return false;
    }

    
}