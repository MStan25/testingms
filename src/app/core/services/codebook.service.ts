import { Injectable, Component } from '@angular/core'
import { Http, Response } from '@angular/http'
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/Observable'
import { MasterService } from '../services/master.service'

@Injectable()
 export class CodebookService {
    constructor(private service: MasterService) { }

    getGenders = function () {
        return this.service.niGetPromise("api/lookups/getgenders");
    }

    getSports = function () {
        return this.service.niGetPromise("api/lookups/getsports");
    }

    getClubs = function () {
        return this.service.niGetPromise("api/lookups/getclubs");
    }

    getSize = function () {
        return this.service.niGetPromise("api/lookups/getsize");
    }

    getCountries = function () {
        return this.service.niGetPromise("api/lookups/getcountries");
    }

    getMeals = function () {
        return this.service.niGetPromise("api/lookups/getmeals");
    }

    getTransport = function () {
        return this.service.niGetPromise("api/lookups/gettransport");
    }
    
}


