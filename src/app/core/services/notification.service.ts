import { Component, Input, Injectable, OnInit } from '@angular/core';
declare const $: any;
import swal from '../../../../node_modules/sweetalert2/dist/sweetalert2.js'
@Injectable()
export class NotificationService {
    constructor() {
    }
    public error(msg: string) {
        swal({
            title: msg,
            text: '',
            type: 'error',
            target: 'body',
            timer: 2000,
            showConfirmButton: false
        })


        // $.notify({
        //     icon: "Greska",
        //     message: msg
        // }, {
        //         type: 'error',
        //         timer: 3000,
        //         target: 'body',
        //         //showProgressbar: true,
        //         z_index:3000,
        //         placement: {
        //             from: "top",
        //             align: "right"
        //         }
        //     });
    }
    public success(msg: string) {



        // $.notify({
        //     icon: "",
        //     message: msg
        // }, {
        //         type: 'success',
        //         timer: 3000,
        //         target: 'body',
        //         z_index:3000,
        //         placement: {
        //             from: "top",
        //             align: "right"
        //         }
        //     });

        swal({
            title: msg,
            text: '',
            type: 'success',
            target: 'body',
            timer: 2000,
            showConfirmButton: false
        })
    }
    public info(msg: string) {

        // $.notify({
        //     icon: "",
        //     message: msg
        // }, {
        //         type: 'info',
        //         timer: 3000,
        //         target: 'body',
        //         z_index:3000,
        //         placement: {
        //             from: "top",
        //             align: "right"
        //         }
        //     });

        swal({
            title: msg,
            text: '',
            type: 'info',
            target: 'body',
            timer: 2000,
            showConfirmButton: false
        })
    }
}
