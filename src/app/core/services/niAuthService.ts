import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http'
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/Observable'
import { AppSettingsModule } from '../../app.settings';

@Injectable()
export class niAuthService {
    headers: Headers;
    options: RequestOptions;

    constructor(private _http: Http) {
        this.constructHeaders();
        this.options = new RequestOptions({ headers: this.headers });
    }
    constructHeaders() {
        var user: any;
        this.headers = new Headers({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PATCH, DELETE',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            'Access-Control-Allow-Credentials': 'true',
        });

        this.options = new RequestOptions({ headers: this.headers });
    }

    private extractData(res: Response) {
        var responseData = res.json();
        if (responseData.result != undefined && responseData.totalCount == undefined) {
            return responseData.result || {};
        } else {
            return responseData || {};
        }
    }
    private handleErrorObservable(error: Response | any) {
        return Observable.throw(error.message || error);
    }
    private handleErrorPromise(error: Response | any) {
        return Promise.reject(error.message || error);
    }
    niGetObservable(url: string): Observable<Response> {
        return this._http.get(AppSettingsModule.ServiceHost + url)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    niPostObservable(url: string, params: any): Observable<Response> {
        return this._http.post(AppSettingsModule.ServiceHost + url, params, this.options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }
    niGetPromise(url: string): Promise<Response> {
        return this._http.get(AppSettingsModule.ServiceHost + url, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }
    niPostPromise(url: string, params: any, ): Promise<Response> {
        return this._http.post(AppSettingsModule.ServiceHost + url, params, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }
}