import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Http, Response } from '@angular/http'
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'

import { niAuthService } from './niAuthService';

@Injectable()
export class LoginService {
    token: boolean = localStorage.getItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E') !== null;

    private showImageSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.token);
    public showImageEmitter: Observable<boolean> = this.showImageSubject.asObservable();

    constructor(private _service: niAuthService) { }

    public login(userLogin: any): Promise<Response> {
        return this._service.niPostPromise("auth/login", userLogin);
    }
    public externallogin(userLogin: any): Promise<Response> {
                return this._service.niPostPromise('auth/externallogin', userLogin);
            }
    public register(userLogin: any): Promise<Response> {
        return this._service.niPostPromise("auth/register", userLogin);
    }


    public refreshtoken(userLogin: any): Promise<Response> {
        return this._service.niPostPromise("auth/refreshtoken", userLogin);
    }
    public recoverAccount(data: any): Promise<Response> {
        return this._service.niPostPromise("auth/accountrecoveryrequest", data);
    }

    public accountRecovery(data: any): Promise<Response> {
        return this._service.niPostPromise("auth/accountrecovery", data);
    }
    public accountVerification(data: any): Promise<Response> {
        return this._service.niPostPromise("auth/verification", data);
    }

    
    
 
 

    public logout() {
        localStorage.removeItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E');
        localStorage.removeItem('2DB9831C-7716-4043-9900-AA00D82B7F61');
        localStorage.removeItem('D84E6855-5C1C-4F69-A036-EDC93ABF9FE8');
        localStorage.removeItem("C483D57E-A4D8-472C-9994-1E6381DE9252");
    }


    public setLoginTempData(email: string, password: string) {
        var loginTempData = { "email": email, "password": password };
        localStorage.setItem('login_temp_data', JSON.stringify(loginTempData));
    }

    /** TOKEN */
    private setTokenSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.token);
    public setTokenEmitter: Observable<boolean> = this.setTokenSubject.asObservable();

    setToken(isTokenCreated: boolean) {
        this.setTokenSubject.next(isTokenCreated);
    }

    /** NAVBAR */
    // private showNavBarSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.token);
    // public showNavBarEmitter: Observable<boolean> = this.showNavBarSubject.asObservable();

    // showNavBar(ifShow: boolean) {
    //     this.showNavBarSubject.next(ifShow);
    // }
}