import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http'


import { HttpClient, HttpResponse } from '@angular/common/http';

import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch'
import { Observable } from 'rxjs/Observable'
import { Router } from '../../../../node_modules/@angular/router';
import { LoginService } from './login.service';
import { AppSettingsModule } from '../../app.settings'
import { NotificationService } from './notification.service';

@Injectable()
export class MasterService {
    headers: Headers;
    user: string;
    constructor(public http: HttpClient, private _http: Http, private router: Router, private loginService: LoginService) {
    }
    private extractData(res: any) {
        var responseData = res;
        if(responseData['token'] !== undefined
        && responseData['token'] != null
        && responseData['tokenId']
        !== undefined && responseData['tokenId'] != null){
            var user = JSON.parse(localStorage.getItem('2DB9831C-7716-4043-9900-AA00D82B7F61'));
            user['password'] = responseData['token'];
            user['tokenId'] = responseData['tokenId'];
            localStorage.setItem('2DB9831C-7716-4043-9900-AA00D82B7F61', JSON.stringify(user));
            localStorage.setItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E', JSON.stringify(user));
        }
        if (responseData['result'] != undefined && responseData['totalCount'] == undefined) {
            return responseData['result'] || {};
        } else {
            return responseData || {};
        }
    }


    niGetPromise(url: string): Observable<Response> {
        return this.http.get(AppSettingsModule.ServiceHost + url)
            .map(this.extractData)
            .catch(
                (error: any) => {
                    if (error.status == 403) {
                        const notify = new NotificationService();
                        notify.error('Vaša trenutna sesija je istekla, molimo da se prijavite za nastavak');
                        localStorage.removeItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E');
                        localStorage.removeItem('2DB9831C-7716-4043-9900-AA00D82B7F61');
                        localStorage.removeItem('D84E6855-5C1C-4F69-A036-EDC93ABF9FE8');
                        localStorage.removeItem('C483D57E-A4D8-472C-9994-1E6381DE9252');
                        this.router.navigate(['login']);

                    } else if (error.status == 500) {

                        const notify = new NotificationService();
                        notify.error('Došlo je do neočekivane pogreške');
                    }

                    return Observable.throw(error);
                }
            );
    }

    niPostPromise(url: string, params: any, recursive: boolean = true): Observable<any> {

        return this.http.post(AppSettingsModule.ServiceHost + url, params)
        .map(this.extractData)
        .catch(
            (error: any) => {
                if (error.status === 403) {
                    const notify = new NotificationService();
                    notify.error('Vaša trenutna sesija je istekla, molimo da se prijavite za nastavak');
                    localStorage.removeItem('D8D22212-072E-4A45-AC69-3B1EB3C87B8E');
                    localStorage.removeItem('2DB9831C-7716-4043-9900-AA00D82B7F61');
                    localStorage.removeItem('D84E6855-5C1C-4F69-A036-EDC93ABF9FE8');
                    localStorage.removeItem('C483D57E-A4D8-472C-9994-1E6381DE9252');
                    this.router.navigate(['login']);

                } else if (error.status === 500) {

                    const notify = new NotificationService();
                    notify.error('Došlo je do neočekivane pogreške');
                }

                return Observable.throw(error);
            }
        );

    }
    niPostPromiseWithoutMap(url: string, params: any, ): Promise<any> {
        return this.http.post(AppSettingsModule.ServiceHost + url, params).toPromise();
    }
}